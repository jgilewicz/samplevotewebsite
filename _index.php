<?php

error_reporting(E_ALL ^ E_NOTICE);


function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strripos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require $fileName;
}
spl_autoload_register('autoload');


class program extends base {

    function Run() {

        $service = (new mysql);
        $sys = (new sys)
                ->Set('All', $service)
                ->Set('Detail', $service)
                ->Set('Vote', $service);
        $this->Set('app', ( new app)
                        ->Set('route', ( new route)
                                ->Set('rules', [ 'main' => 'list', 'detail' => 'detail', 'vote' => 'vote', 'not_found' => 'not_found']))
                        ->Set('responses', [
                            'list' => ( new response)
                            ->Set('name', 'list')
                            ->Set('page', ( new page)
                                    ->Set('action', ( new base)
                                            ->Set('name', 'Detail')
                                            ->Set('sys', $sys))),
                            'detail' => ( new response)
                            ->Set('name', 'detail')
                            ->Set('error', 'list')
                            ->Set('args', [ 'id'])
                            ->Set('request', ( new request)
                                    ->Set('match', [ 'id' => '/^\d{1,31}$/']))
                            ->Set('page', ( new page)
                                    ->Set('action', ( new base)
                                            ->Set('name', 'Detail')
                                            ->Set('sys', $sys))),
                            'vote' => ( new response)
                            ->Set('name', 'vote')
                            ->Set('error', 'vote_rejected')
                            ->Set('args', [ 'id'])
                            ->Set('request', (new request)
                                    ->Set('match', [ 'id' => [ 'name' => '/^\d{1,31}$/']]))
                            ->Set('page', ( new page)
                                    ->Set('action', ( new base)
                                            ->Set('name', 'Vote')
                                            ->Set('sys', $sys)))]));
        $this->app->Run();
    }

}

echo (new program)->Run()->v;

?>
