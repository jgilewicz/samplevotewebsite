<?php

require_once 'init.php';

use Mylib\Common\Base;
use Mylib\Common\Page;
use Mylib\Common\Sys;
use Mylib\Common\App;
use Mylib\Common\Route;
use Mylib\Common\Response;
use Mylib\Common\Request;
use Mylib\Service\Mysqltest;

$service = (new Mysqltest);
//xdebug_break();
$sys = (new Sys)
            ->Set('DetailCandidate', $service)
            ->Set('DetailParty', $service)
            ->Set('All', $service)
            ->Set('Search', $service)
            ->Set('AllParties', $service)
            ->Set('Vote', $service)
            ->Set('Result', $service);


$page_list_parties = (new Page)
                            ->Set('template', 'list_parties')
                            ->Set('action', ( new Base)
                                    ->Set('name', 'AllParties')
                                    ->Set('sys', $sys));

$page_list_candidates = (new Page)
                            ->Set('template', 'list_candidates')
                            ->Set('action', ( new Base)
                                    ->Set('name', 'All')
                                    ->Set('sys', $sys));


$page_form_vote = (new Page)
        ->Set('template', 'form_vote')
        ->Set('pages', ['list_candidates' => $page_list_candidates,
                        'form_search' => (new Page)->Set('template', 'form_search')]);

$page_detail = (new Page)
        ->Set('template', 'detail')
        ->Set('action', (new Base)
                ->Set('name', 'Detail')
                ->Set('sys', $sys));

$page_result = (new Page)
        ->Set('template', 'result')
        ->Set('action', (new Base)
                ->Set('name', 'Result')
                ->Set('sys', $sys));

$page_vote = (new Page)
        ->Set('template', 'vote')
        ->Set('action', (new Base)
                ->Set('name', 'Vote')
                ->Set('sys', $sys));

$page_not_found = (new Page)->Set('template', 'not_found');

$page_menu = (new Page)->Set('template', 'menu');
        
$app = (new App)
            ->Set('route', (new Route)
                    ->Set('rules', [
                        'main' => 'vote',
                        'list' => 'list_candidates',
                        'listparties' => 'list_parties',
                        'vote' => 'vote',
                        'result' => 'result',
                        'not_found' => 'not_found',
                        ]))
            ->Set('responses', [
                'vote' => (new Response)
                    ->Set('name', 'vote')
                    ->Set('error',  (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_form_vote
                                    ]))
                    ->Set('args', [ 'first_name',
                                    'last_name',
                                    'pesel',
                                    'candidate_id',
                                    'search'])
                    ->Set('request', (new Request)
                            ->Set('match', [
//                                'first_name' => '/^[\w ]{3,250}$/',
                                'first_name' => '//',
//                                'last_name' => '/^[\w ]{3,250}$/',
                                'last_name' => '//',
//                                'pesel' => '/^\d{9}$/',
                                'pesel' => '//',
//                                'candidate_id' => '/^\d{1,31}$/',
                                'candidate_id' => '//',
//                                'search' => '/^(\s*\w{3,31}\s*\w{3,31}\s*|)$/']))
                                'search' => '//']))
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_vote,
                                ])),
                'list_parties' => (new Response)
                    ->Set('name', 'list_parties')
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_list_parties,
                                ])),
                'list_candidates' => (new Response)
                    ->Set('name', 'list_candidates')
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_list_candidates,
                                ])),
                'not_found' => (new Response)
                    ->Set('name', 'not_found')
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_not_found,
                                ])),
                'result' => (new Response)
                    ->Set('name', 'result')
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_result,
                                ])),
                ]);
//var_dump($app->responses['list']);
echo $app->Run()->response->page->v;

?>
