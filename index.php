<?php
//session_start();
ini_set('output_buffering','On');
error_reporting(E_ALL ^ E_NOTICE);
require_once 'autoload.php';

use Mylib\Common\Base;
use Mylib\Common\Page;
use Mylib\Common\Sys;
use Mylib\Common\App;
use Mylib\Common\Route;
use Mylib\Common\Response;
use Mylib\Common\Request;
use Mylib\Service\Mysql;

$service = (new Mysql);
//xdebug_break();
$sys = (new Sys)
            ->Set('DetailCandidate', $service)
            ->Set('DetailParty', $service)
            ->Set('All', $service)
            ->Set('Search', $service)
            ->Set('AllParties', $service)
            ->Set('Vote', $service)
            ->Set('AdminParties', $service)
            ->Set('AdminCandidates', $service)
            ->Set('Result', $service);

//$page_list = (new Page)
//                    ->Set('template', 'list')
//                    ->Set('action', ( new Base)
//                            ->Set('name', 'All')
//                            ->Set('sys', $sys));

$page_list_parties = (new Page)
                            ->Set('template', 'list_parties')
                            ->Set('action', ( new Base)
                                    ->Set('name', 'AllParties')
                                    ->Set('sys', $sys));

$page_list_candidates = (new Page)
                            ->Set('template', 'list_candidates')
                            ->Set('action', ( new Base)
                                    ->Set('name', 'All')
                                    ->Set('sys', $sys));

$page_list_parties_admin = (new Page)
                            ->Set('template', 'list_parties_admin')
                            ->Set('action', ( new Base)
                                    ->Set('name', 'AllParties')
                                    ->Set('sys', $sys));

$page_list_candidates_admin = (new Page)
                            ->Set('template', 'list_candidates_admin')
                            ->Set('action', ( new Base)
                                    ->Set('name', 'All')
                                    ->Set('sys', $sys));

$page_form_vote = (new Page)
        ->Set('template', 'form_vote')
        ->Set('pages', ['list_candidates' => $page_list_candidates,
                        'form_search' => (new Page)->Set('template', 'form_search')]);

# $page_form_vote = (new Page)
#         ->Template('form_vote')
#         ->Pages(['list_candidates' => $page_list_candidates,
#                         'form_search' => (new Page)->Template('form_search')]);

$page_form_admin_parties = (new Page)
        ->Set('template', 'form_admin_parties')
        ->Set('action', (new Base)
                ->Set('name', 'DetailParty')
                ->Set('sys', $sys))
        ->Set('pages', ['list_parties' => $page_list_parties_admin]);

$page_form_admin_candidates = (new Page)
        ->Set('template', 'form_admin_candidates')
        ->Set('action', (new Base)
                ->Set('name', 'DetailCandidate')
                ->Set('sys', $sys))
        ->Set('pages', ['list_candidates' => $page_list_candidates_admin]);

$page_detail = (new Page)
        ->Set('template', 'detail')
        ->Set('action', (new Base)
                ->Set('name', 'Detail')
                ->Set('sys', $sys));

$page_result = (new Page)
        ->Set('template', 'result')
        ->Set('action', (new Base)
                ->Set('name', 'Result')
                ->Set('sys', $sys));

$page_vote = (new Page)
        ->Set('template', 'vote')
        ->Set('action', (new Base)
                ->Set('name', 'Vote')
                ->Set('sys', $sys));

$page_admin_parties = (new Page)
        ->Set('template', 'admin_parties')
        ->Set('action', (new Base)
                ->Set('name', 'AdminParties')
                ->Set('sys', $sys));

$page_admin_candidates = (new Page)
        ->Set('template', 'admin_candidates')
        ->Set('action', (new Base)
                ->Set('name', 'AdminCandidates')
                ->Set('sys', $sys));

$page_not_found = (new Page)->Set('template', 'not_found');

$page_menu = (new Page)->Set('template', 'menu');
        
$app = (new App)
            ->Set('route', (new Route)
                    ->Set('rules', [
                        'main' => 'list_candidates',
                        'list' => 'list_candidates',
                        'listparties' => 'list_parties',
                        'detail' => 'detail',
                        'vote' => 'vote',
                        'adminparties' => 'admin_parties',
                        'admincandidates' => 'admin_candidates',
                        'result' => 'result',
                        'not_found' => 'not_found',
                        ]))
            ->Set('responses', [
                'detail' => (new Response)
                    ->Set('name', 'detail')
                    ->Set('error', (new Page)
                            ->Set('template', 'layout')
                            ->Set('head', $page_menu)
                            ->Set('content', $page_form))
                    ->Set('args', [ 'id'])
                    ->Set('request', (new Request)
                            ->Set('match', [ 'id' => '/^\d{1,31}$/']))
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('head', $page_menu)
                            ->Set('content', $page_detail)),
                'vote' => (new Response)
                    ->Set('name', 'vote')
                    ->Set('error',  (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_form_vote
                                    ]))
                    ->Set('args', [ 'first_name',
                                    'last_name',
                                    'pesel',
                                    'candidate_id',
                                    'search'])
                    ->Set('request', (new Request)
                            ->Set('match', [
                                'first_name' => '/^[\w ]{3,250}$/',
                                'last_name' => '/^[\w ]{3,250}$/',
                                'pesel' => '/^\d{9}$/',
                                'candidate_id' => '/^\d{1,31}$/',
                                'search' => '/^(\s*\w{3,31}\s*\w{3,31}\s*|)$/']))
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_vote,
                                ])),
                'admin_parties' => (new Response)
                    ->Set('name', 'admin_parties')
                    ->Set('error',  (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_form_admin_parties
                                    ]))
                    ->Set('args', [ 'id', 'name', 'photo_url', 'submit'])
                    ->Set('request', (new Request)
                            ->Set('match', [
                                'id' => '/^(\d{1,31}|)$/',
                                'name' => '/^[\w ]{3,250}$/',
//                                'photo_url' => '/^((https?|ftp)://)?(([^:\n\r]+):([^@\n\r]+)@)?((www\.)?([^/\n\r]+))/?([^?\n\r]+)?\??([^#\n\r]*)?#?([^\n\r]*)$/',
                                'photo_url' => '/.+/',
                                'submit' => '/.+/',
                                ]))
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_admin_parties,
                                ])),
                'admin_candidates' => (new Response)
                    ->Set('name', 'admin_candidates')
                    ->Set('error',  (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_form_admin_candidates
                                    ]))
                    ->Set('args', [ 'id', 'parties_id', 'order_number', 'first_name', 'last_name', 'description', 'photo_url', 'submit'])
                    ->Set('request', (new Request)
                            ->Set('match', [
                                'id' => '/^(\d{1,31}|)$/',
                                'parties_id' => '/^(\d{1,31})$/',
                                'order_number' => '/^(\d{1,31}|)$/',
                                'first_name' => '/^[\w ]{3,250}$/',
                                'last_name' => '/^[\w ]{3,250}$/',
                                'description' => '/^[\w .,]{3,1020}$/',
//                                'photo_url' => '/^((https?|ftp)://)?(([^:\n\r]+):([^@\n\r]+)@)?((www\.)?([^/\n\r]+))/?([^?\n\r]+)?\??([^#\n\r]*)?#?([^\n\r]*)$/',
                                'photo_url' => '/.+/',
                                'submit' => '/.+/',
                                ]))
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_admin_candidates,
                                ])),
                'list_parties' => (new Response)
                    ->Set('name', 'list_parties')
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_list_parties,
                                ])),
                'list_candidates' => (new Response)
                    ->Set('name', 'list_candidates')
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_list_candidates,
                                ])),
                'not_found' => (new Response)
                    ->Set('name', 'not_found')
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_not_found,
                                ])),
                'result' => (new Response)
                    ->Set('name', 'result')
                    ->Set('page', (new Page)
                            ->Set('template', 'layout')
                            ->Set('pages', [
                                'head' => $page_menu,
                                'content' => $page_result,
                                ])),
                ]);
//var_dump($app->responses['list']);
echo $app->Run()->response->page->v;

?>
