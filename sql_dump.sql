-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 29, 2014 at 07:45 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `wpzadanie`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE IF NOT EXISTS `candidates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parties_id` int(11) NOT NULL,
  `order_number` int(11) NOT NULL DEFAULT '0',
  `first_name` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `last_name` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `description` varchar(1020) COLLATE utf8_polish_ci NOT NULL,
  `photo_url` varchar(510) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parties_id_2` (`parties_id`,`order_number`),
  KEY `first_name` (`first_name`,`last_name`),
  KEY `parties_id` (`parties_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`id`, `parties_id`, `order_number`, `first_name`, `last_name`, `description`, `photo_url`) VALUES
(9, 1, 1, 'Wojciech', 'Nowak', 'Popieram VAT w wysokosci 21 procent.', 'http://cdn.graphicsfactory.com/clip-art/image_files/image/0/1346060-2699-Royalty-Free-Emoticon-With-Sunglasses.jpg'),
(11, 2, 1, 'Jan', 'Kowalski', 'Popieram VAT w wysokosci 19 procent.', 'http://www.polyvore.com/cgi/img-thing?.out=jpg&size=l&tid=4753413'),
(12, 3, 1, 'Stanislaw', 'Malinowski', 'Popieram VAT w wysokosci 17 procent.', 'http://images.sodahead.com/polls/002484927/854403010_blank_face_smiley_sticker_p217085515084213312z85xz_400_answer_2_xlarge.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `parties`
--

CREATE TABLE IF NOT EXISTS `parties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `photo_url` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `photo_url` (`photo_url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `parties`
--

INSERT INTO `parties` (`id`, `name`, `photo_url`) VALUES
(1, 'Partia Jeden', 'http://www.decodeunicode.org/data/glyph/196x196/2460.gif'),
(2, 'Partia Dwa', 'http://www.i2symbol.com/images/symbols/style-digits/circled_digit_two_u2461_icon_256x256.png'),
(3, 'Partia Trzy', 'http://www.i2symbol.com/images/abc-123/3/circled_digit_three_u2462_icon_256x256.png');

-- --------------------------------------------------------

--
-- Table structure for table `voters`
--

CREATE TABLE IF NOT EXISTS `voters` (
  `pesel` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `first_name` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `last_name` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`pesel`),
  KEY `candidate_id` (`candidate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=321321322 ;

--
-- Dumping data for table `voters`
--

INSERT INTO `voters` (`pesel`, `candidate_id`, `first_name`, `last_name`) VALUES
(123123123, 11, 'Marcin', 'Kowalski'),
(123123124, 11, 'Marcin', 'Kowalski'),
(321321321, 9, 'Maria', 'Nowak');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `candidates`
--
ALTER TABLE `candidates`
  ADD CONSTRAINT `candidates_ibfk_1` FOREIGN KEY (`parties_id`) REFERENCES `parties` (`id`);

--
-- Constraints for table `voters`
--
ALTER TABLE `voters`
  ADD CONSTRAINT `voters_ibfk_1` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`);
