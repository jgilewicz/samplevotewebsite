<?php

namespace Mylib\Common;

class Page extends Base {

    function Run() {
        foreach ((array)$this->pages as $name => $page) 
            $this->data[$name] = $page->Set('data', $this->data)->Run()->v;
        if ($this->action) $this->Set('data', $this->action->sys
                ->Set('action', $this->action->name)
                ->Set('data', $this->data)
                ->Run()
                ->data);
        $this->Build();
        return $this;
    }

    function Build() {
        foreach ((array)$this->data as $k => $v) $$k = $v;
        ob_start();
        include 'page/' . $this->template . '.php';
        $this->v = ob_get_clean();
        return $this;
    }

}

?>
