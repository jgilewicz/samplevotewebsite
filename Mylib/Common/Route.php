<?php

namespace Mylib\Common;

class Route extends Base {

    function GetResponse() {
        if (!isset($_GET[c])) return $this->rules['main'];
        foreach ($this->rules as $name => $rule) if (!strcmp($name, $_GET[c])) return $rule;
        return $this->rules['not_found'];
    }

}

?>
