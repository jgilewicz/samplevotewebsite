<?php

namespace Mylib\Common;

class Response extends Base {

    function Run() {
        if ($this->request) {
        $this->request->Run();
        if ($this->request->invalid && $this->error)
            $this->Set('page', $this->error);
//        else
//            $this->page->Set('template', $this->name);
        $this->SetArgs();
        }
        $this->page->Run();
        return $this;
    }

    function SetArgs() {
        foreach ((array)$this->args as $arg => $name)
            if (is_string($arg))
                $this->page->data[$arg] = $this->request->Get($name);
            else
                $this->page->data[$name] = $this->request->Get($name);
        return $this;
    }
}

?>
