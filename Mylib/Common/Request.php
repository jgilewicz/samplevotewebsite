<?php

namespace Mylib\Common;

class Request extends Base {

    function Run() {
        foreach ($this->match as $k => $v)
            if (preg_match($v, $_REQUEST[$k]))
                $this->input[$k] = $_REQUEST[$k];
            else
                $this->Set('invalid', true);
        return $this;
    }

    function Get($param) {
        if (isset($this->input[$param]))
            return $this->input[$param];
        return null;
    }

}

?>
