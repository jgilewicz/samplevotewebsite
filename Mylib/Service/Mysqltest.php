<?php

namespace Mylib\Service;
use Mylib\Common\Base;

require_once 'PHP-MySQLi-Database-Class-master/MysqliDb.php';

class Mysqltest extends Base {
    
    private function GetDb() {
        return $db = new \Mysqlidb ('localhost', 'root', 'test', 'wpzadanie');
    }
    
    private function CandidatesOrderNumbersGrow ($parties_id, $start, $end) 
    {
        $db = $this->GetDb();
        for ($i = $end; $i >= $start; $i--) 
            $db
                ->where('parties_id', $parties_id)
                ->where('order_number', $i)
                ->update('candidates', ['order_number' => $i + 1]);
    }
    
    private function CandidatesOrderNumbersShrink ($parties_id, $start, $end) 
    {
        $db = $this->GetDb();
        for ($i = $start + 1; $i <= $end; $i++) 
            $db
                ->where('parties_id', $parties_id)
                ->where('order_number', $i)
                ->update('candidates', ['order_number' => $i - 1]);
    }
    
    private function CandidateOrderNumberAdd ($parties_id, $order_number) 
    {
        $db = $this->GetDb();
        $db->where('parties_id', $parties_id);
        $members = $db->getOne('candidates', 'count(id) as count');
        if (($order_number < 1) || ($order_number > $members['count']))
            return $members['count'] + 1;
        $this->CandidatesOrderNumbersGrow
                ($parties_id, $order_number, $members['count']);
        return $order_number;
    }

    private function CandidateOrderNumberRemove ($parties_id, $order_number) 
    {
        $db = $this->GetDb();
        $db->where('parties_id', $parties_id);
        $members = $db->getOne('candidates', 'count(id) as count');
        $this->CandidatesOrderNumbersShrink
                ($parties_id, $order_number, $members['count'] + 1);
    }

    private function CandidateOrderNumberMove ($parties_id, $from, $to) 
    {
        $db = $this->GetDb();
        $db->where('parties_id', $parties_id);
        $members = $db->getOne('candidates', 'count(id) as count');
        if (($to < 1) || ($to > $members['count'])) $to = $members['count'] + 1;
        $db
                ->where('parties_id', $parties_id)
                ->where('order_number', $from)
                ->update('candidates', ['order_number' => 0]);
        if ( $from < $to) {
            $this->CandidatesOrderNumbersShrink ($parties_id, $from, $to);
        } else {
            $this->CandidatesOrderNumbersGrow ($parties_id, $to, $from - 1);
        }
        $db
                ->where('parties_id', $parties_id)
                ->where('order_number', 0)
                ->update('candidates', ['order_number' => $to]);
        return $to;
    }

    public function AdminCandidates() {
        $db = $this->GetDb();
        $id = $this->data['id'];
        
        if ($id && ($this->data['submit'] == 'usun')) {
            $this->DetailCandidate();
            $db->where('id', $id);
            $ret = $db->delete('candidates');
            $this->CandidateOrderNumberRemove(
                    $this->data['DetailCandidate']['parties_id'],
                    $this->data['DetailCandidate']['order_number']);
            $this->data['info'] = ( $ret)
                    ?'Kasowanie kandydata zakończone sukcesem.'
                    :'Kasowanie kandydata zakończone błędem.';
            return $this;
        }
        
        if ($id) {
            $this->DetailCandidate();
            $this->data['order_number'] = $this->CandidateOrderNumberMove(
                    $this->data['DetailCandidate']['parties_id'],
                    $this->data['DetailCandidate']['order_number'],
                    $this->data['order_number']);
            $db->where ("id", $id);
            $ret = $db->update('candidates', 
                    array_intersect_key($this->data,
                            array_flip(['first_name', 
                                'last_name',
                                'description',
                                'photo_url',
                                'order_number',
                                'parties_id'])));
            $this->data['info'] = ( $ret)
                    ?'Edycja kandydata zakończona sukcesem.'
                    :'Edycja kandydata zakończona błędem.';
            return $this;
        }
        
        $this->data['order_number'] = $this->CandidateOrderNumberAdd(
                $this->data['parties_id'],
                $this->data['order_number']);
        $ret = $db->insert('candidates', 
                    array_intersect_key($this->data,
                            array_flip(['first_name', 
                                'last_name',
                                'description',
                                'photo_url',
                                'order_number',
                                'parties_id'])));
        $this->data['info'] = ($ret)
                ?'Dodawanie kandydata zakończone sukcesem.'
                :'Dodawanie kandydata zakończone błędem.';
                echo $db->getLastQuery();
        return $this;
    }

    public function AdminParties() {
        $db = $this->GetDb();
        $id = $this->data['id'];
        if ($id && ($this->data['submit'] == 'usun')) {
            $db->where('id', $id);
            $ret = $db->delete('parties');
            $this->data['info'] = ( $ret)
                    ?'Kasowanie partii zakończone sukcesem.'
                    :'Kasowanie partii zakończone błędem.';
            return $this;
        }
        
        if ($id) {
            $db->where ("id", $id);
            $ret = $db->update('parties', 
                    array_intersect_key($this->data,
                            array_flip(['name', 
                                'photo_url'])));
            $this->data['info'] = ( $ret)
                    ?'Edycja partii zakończona sukcesem.'
                    :'Edycja partii zakończona błędem.';
            return $this;
        }
        
        $ret = $db->insert('parties', 
                    array_intersect_key($this->data,
                            array_flip(['name', 
                                'photo_url'])));
        $this->data['info'] = ($ret)
                ?'Dodawanie partii zakończone sukcesem.'
                :'Dodawanie partii zakończone błędem.';
        return $this;
    }

    public function All() {
        if ($this->data['search']) 
            $this->data['All'] = $this->Search()->data['Search'];
        else $this->data['All'] = $this
                ->GetDb()
                ->join('parties p', "c.parties_id=p.id", 'left')
                ->orderBy('p.name', 'asc')
                ->orderBy('c.order_number', 'asc')
                ->get('candidates c', null, 'c.*, p.name as party_name');
        return $this;
    }
    
    public function Search() {
        $search = explode( ' ', trim($this->data['search']));
        $this->data['Search'] = $this
                ->GetDb()
                ->join('parties p', "c.parties_id=p.id", 'left')
                ->where('first_name', ['LIKE' => "%{$search[0]}%"])
                ->where('last_name', ['LIKE' => "%{$search[1]}%"])
                ->orderBy('p.name', 'asc')
                ->orderBy('c.order_number', 'asc')
                ->get('candidates c', null, 'c.*, p.name as party_name');
        return $this;
    }
    
    public function DetailCandidate() {
        if ($this->data['id'])
        $this->data['DetailCandidate'] = $this
                ->GetDb()
                ->where ("id", $this->data['id'])
                ->getOne ('candidates');
        return $this;
    }
    
    public function DetailParty() {
        if ($this->data['id'])
        $this->data['DetailParty'] = $this
                ->GetDb()
                ->where ("id", $this->data['id'])
                ->getOne ('parties');
        return $this;
    }
    
    public function AllParties() {
        $this->data['AllParties'] = $this->GetDb()->get('parties');
        return $this;
    }
    
    public function Result() {
        $db = $this->GetDb();
        $db->groupBy('candidate_id');
        $db->join("candidates c", "v.candidate_id=c.id", "LEFT");
        $this->data['Result'] = $db
                ->get('voters v',
                        null,
                        'c.last_name, c.first_name, v.candidate_id, '
                        . 'count(v.pesel) as votes');
        return $this;
    }

    public function Vote() {
        $this->data['first_name'] = 'qwe';
        $this->data['last_name'] = 'asd';
//        $this->data['pesel'] = 111222333;
        $this->data['pesel'] = $this->GetDb()->getOne('voters', 'max(pesel) as pesel')['pesel'];
        $this->data['candidate_id'] = $this->GetDb()->getOne('candidates', 'max(id) as id')['id'];
        for ($i = 0; $i < 300; $i++) {
        $this->data['pesel']++;
        
        $db = $this->GetDb();
        $ret = $db->insert('voters', 
                    array_intersect_key($this->data,
                            array_flip(['first_name',
                                'last_name',
                                'pesel',
                                'candidate_id'])));
        $this->data['info'] = ($ret)
                ?$i . ': Dziękujemy, głos został oddany.'
                :$i . ': Głos nieważny. Spróbuj ponownie.';
        
        $db = $this->GetDb();
        $ret = $db->insert('candidates', 
                            [
                            'first_name' => 'Asd' . $this->data['pesel'], 
                                'last_name' => 'Qwe' . $this->data['pesel'],
                                'description'=> 'description' . $this->data['pesel'],
                                'photo_url' => 'htpp://example.com',
                                'order_number' =>  $this->data['pesel'],
                                'parties_id' => 1]);
//        $this->data['candidate_id'] = $ret;
            
        if (!$ret) {
        echo $db->getLastQuery();
        echo $db->getLastError();
        break;
        }
        }
        return $this;
    }

}

?>
