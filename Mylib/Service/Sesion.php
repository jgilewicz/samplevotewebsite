<?php

namespace Mylib\Service;
use Mylib\Common\Base;

class Session extends Base {

    private $candidates = [
        [],
        ['Jan Kowalski: Partia Numer Jeden'],
        ['Stanisław Nowak: Partia Numer Dwa'],
        ['Wojciech Malinowaski: Partia Numer Trzy'],
    ];
    
    public function All() {
        $this->data['All'] = $this->candidates;
        return $this;
    }

    public function Result() {
        $count = array_count_values((array)$_SESSION['Vote']);
        foreach ( $this->candidates as $number => $candidate)
            if (count($candidate))
                $this->data['Result'][$number] =
                            [$candidate[0], $count[$number]];
        return $this;
    }

    public function Vote() {
        if (!$_SESSION['Vote'][$this->data['pesel']]
                && $this->data['id'] > 0 
                && $this->data['id'] < count($this->candidates)) {
            $_SESSION['Vote'][$this->data['pesel']] = $this->data['id'];
            $this->data['info'] = 'Dziękujemy, głos został oddany';
        return $this;
            }
        $this->data['info'] = 'Spróbuj ponownie';
        error_log(
                'Głos nieważny: ' 
                . $this->data['pesel'] 
                . ' - ' . $this->data['id']
                );
        return $this;
    }

}

?>
